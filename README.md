## HTMLify Ordered List
This is a Python script that takes input text containing ordered list items and converts it into HTML format. The script defines two functions, htmlify_ordered_list and htmlify_ordered_list_2, to perform the conversion. The script also prompts the user to enter the text, processes it, and saves the converted result to a file named "converted_result.txt".

## How to Use
1. Make sure you have Python installed on your system.

2. Copy the provided Python script into a new file named htmlify_ordered_list.py.

3. Run the script using the following command:
 - python htmlify_ordered_list.py

4. The script will prompt you to enter the input text. You can paste or type the text and press Enter.

5. To finish entering the text and start the conversion, enter --- on a new line and press Enter.

6. The script will process the input text and display the converted result on the console.

7. The converted result will also be saved in a file named "converted_result.txt" in the same directory as the script.

## Functions
htmlify_ordered_list(text)
This function takes a string text as input, which contains the unordered list items, and converts it into an HTML formatted string. It detects the ordered list items (lines starting with a number followed by a period and a space), wraps them in <li> tags, and if applicable, wraps the entire list in <ol> tags.

htmlify_ordered_list_2(input_text)
This function takes an HTML formatted string input_text as input and performs additional processing to ensure that the ordered list is correctly wrapped within <ol> tags. This is done by finding the first occurrence of <li> and wrapping it in an opening <ol><li> tag and finding the last occurrence of </li> and wrapping it in a closing </li></ol> tag.

## Example
Suppose you want to convert the following input text into an HTML formatted ordered list:

1. First item
2. Second item
3. Third item

Some text here.

4. Fourth item
5. Fifth item

After running the script and entering the above input text, the converted result will be:

<ol>
<li>First item</li>
<li>Second item</li>
<li>Third item</li>
</ol>
Some text here.
<ol>
<li>Fourth item</li>
<li>Fifth item</li>
</ol>

The converted result will also be saved to "converted_result.txt".
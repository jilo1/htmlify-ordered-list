import re

def htmlify_ordered_list(text):
    # split text into lines
    lines = text.split('\n')

    # initialize a new list to hold processed lines
    processed_lines = []

    # flag to detect if we're in an ordered list
    in_ordered_list = False

    # initialize an empty string to hold the list items
    list_items = ""

    # iterate over each line
    for line in lines:
        # match any line that starts with a number followed by a period and a space
        match = re.match(r'(\d+)\. (.*)', line)

        if match:
            # If we are not in an ordered list, start the ordered list
            if not in_ordered_list:
                in_ordered_list = True

            # Add the <li> item to the list_items string
            list_items += f"<li>{match.group(2)}</li>"
        else:
            # If the line does not match the ordered list pattern and we were in an ordered list, close the ordered list
            if in_ordered_list:
                # processed_lines.append("<ol>")
                processed_lines.append(list_items)
                # processed_lines.append("</ol>")
                in_ordered_list = False
                # Reset list_items for the next ordered list, if any
                list_items = ""

            # Add the line as it is to the processed_lines list
            processed_lines.append(line)

    # If we ended with an open ordered list, close it
    if in_ordered_list:
        # processed_lines.append("<ol>")
        processed_lines.append(list_items)
        # processed_lines.append("</ol>")

    # Join the processed lines into a single string
    return '\n'.join(processed_lines)

def htmlify_ordered_list_2(input_text):
    # Find the first <li> and replace it with <ol><li>
    first_li_index = input_text.find("<li>")
    if first_li_index != -1:
        input_text = input_text[:first_li_index] + "<ol><li>" + input_text[first_li_index + 4:]

    # Find the last </li> and replace it with </li></ol>
    last_li_index = input_text.rfind("</li>")
    if last_li_index != -1:
        input_text = input_text[:last_li_index] + "</li></ol>" + input_text[last_li_index + 5:]

    return input_text

# Get the prompt from the user
sentinel = '---'
print(f"Input/paste Text below (Enter {sentinel} on a new line when finished):")
user_prompt_lines = []

while True:
    line = input()
    if line == sentinel:
        break
    user_prompt_lines.append(line)

text = "\n".join(user_prompt_lines)

output_html = htmlify_ordered_list(f"{text}")
output_html_2 = htmlify_ordered_list_2(f"{output_html}")

# Save the converted result to a file
output_filename = "converted_result.txt"
with open(output_filename, 'w') as output_file:
    output_file.write(output_html_2)

print("Converted Result:\n",output_html_2)
print("The converted result has been saved to", output_filename)
